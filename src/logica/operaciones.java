package logica;

 public class operaciones {
 public long factorial(long n) {
	 if (n < 0) {
         throw new IllegalArgumentException("No se permite calcular el factorial de un número negativo.");
     }

     long result = 1;
     for (int i = 1; i <= n; i++) {
         if (result > Long.MAX_VALUE / i) {
             throw new ArithmeticException("El resultado del factorial excede la capacidad del tipo long.");
         }
         result *= i;
         
     }
     return result;
    }
 public double division(double dividendo, double divisor) {
     if (divisor == 0) {
         throw new ArithmeticException("Error: división por cero.");
 }
     return dividendo/divisor;
 }
}